## Rotman Algo

This is the repository for rotman algo trading case strategies.  Since RIT
can only be accessed using a Windows OS, Mac users will have to install Windows.
If you don't have enough space to install windows, you can use mfe-rit machine.

Repo has two main python packages:

lib: reusable library code for generating signals.

strategy:  This package contains the strategy code and uses the modules
from lib to create new signals.

### Dev setup
1. Create isolated environment to avoid compatibility issues  with global python packages.
Create a new conda environment with given setup. To install conda follow this
[link](https://docs.conda.io/projects/conda/en/latest/user-guide/install/)

For windows: Launch conda commandline prompt from start menu in windows. Do not use normal
command line.
```bash
#create env
cd rotman-algo
conda env create -f environment.yml
source activate rotm-env
```

### Git instructions

If you are using mfe-rit machine, then you don't need to setup. Follow below
steps if you using your own setup.

Follow this link on your machine to install git [link](https://www.computerhope.com/issues/ch001927.htm)

git clone to get repository on your machine.

```
git clone git@gitlab.com:rohit.chandel/rotman-algo.git
```


### Configuration 
Algo strategy requires client address and api key. Login to rit client and
click on "API" to get this information.
Modify algo.json with your api key and rit client port(usually you won't require to
change port as its 9999 by default)

Change logging_level to critical for production as we don't want to slowdown
our code due to logging. You can set it as INFO during development and
debugging.

algo.json also contains the parameters of different strategies. Change these
according to market conditions and rerun strategies.


### Run instructions

```
# make sure you are in top level rotman_algo folder with  conda environment activated

python -m algo

```

Above command will run all strategies with given configurations in algo.json



### Development instruction
If you are  using your own machine for development,  clone  the repository.
Activate conda env and launch spyder
For mfe rit machine, go to C:\Users\mfeadmin\rotman from anaconda prompt,
activate environment and launch spyder.
Open rotman-algo project in spyder from project menu.

```
spyder
```



