import requests
import logging
from time import sleep
# algo modules
import algo
import algo.lib.utils as utils
import algo.lib.om as om
import algo.lib.mm as mm
import algo.lib.research as research
import numpy as np
import pandas as pd

class transact():
    def __init__(self,session):
        self.session = session
    
    def BUY_BULL(self,amount = 10000):
     om.bulk_submit({'ticker': 'BULL', 'type': 'MARKET', 'quantity': amount,
                            'action': 'BUY'}, self.session)   


    def SELL_BULL(self,amount = 10000):
     om.bulk_submit({'ticker': 'BULL', 'type': 'MARKET', 'quantity': amount,
                            'action': 'SELL'}, self.session)   

    def BUY_BEAR(self,amount = 10000):
     om.bulk_submit({'ticker': 'BEAR', 'type': 'MARKET', 'quantity': amount,
                            'action': 'BUY'}, self.session)   

    def BUY_RITC(self,amount = 10000):
     om.bulk_submit({'ticker': 'RITC', 'type': 'MARKET', 'quantity': amount,
                            'action': 'BUY'}, self.session)   

    def SELL_BEAR(self,amount = 10000):
     om.bulk_submit({'ticker': 'BEAR', 'type': 'MARKET', 'quantity': amount,
                            'action': 'SELL'}, self.session)   

    def SELL_RITC(self,amount = 10000):
     om.bulk_submit({'ticker': 'RITC', 'type': 'MARKET', 'quantity': amount,
                            'action': 'SELL'}, self.session)        
    
    def CLOSE_ALL(self):
        dfsec = pd.DataFrame(om.sec_info(self.session))
        secs = ['USD', 'RITC', 'BULL', 'BEAR']
        for sec in secs:
            sec_pos = dfsec.loc[dfsec['ticker'] == sec, 'position'].values[0]
            if sec < 0:
                direction = 'BUY'
            else:
                direction = 'SELL'
            om.bulk_submit({'ticker': sec, 'type': 'MARKET', 'quantity': abs(sec_pos),
                            'action': direction}, self.session) 

#def execute_trade(ticker, direction, amount, session):
#     om.bulk_submit({'ticker': ticker, 'type': 'MARKET', 'quantity': amount,
#                            'action': direction}, session)
    
cfg = algo.config
api_key = {'X-API-key': cfg['api_key']}
cfg = algo.config
session =  requests.Session()
session.headers.update(api_key)


#execute_trade('RITC', 'SELL', 10000, session)

T = transact(session)
    

    