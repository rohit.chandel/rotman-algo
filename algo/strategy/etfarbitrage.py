"""
ETF arbitrage using cointegration
"""
import algo
import numpy as np
import algo.lib.utils as utils
import algo.lib.om as om
import pandas as pd
import algo.lib.ts as ts
import time

dfportfolio = ts.make_ts(['BID', 'ASK'])

POSITION = 0

HEDGE_RATIO = 2

SCALE_LEVELS = np.array(range(1, 11))/10

def init():
    global POSITION
    global dfportfolio
    global SCALE_LEVELS
    dfportfolio = ts.make_ts(['BID', 'ASK'])
    POSITION = 0
    SCALE_LEVELS = np.array(range(1, 11))/10

    
def vwap(ticker, s, size, dfbook, bid = 1):
    """
    0 for bid 1 for ask
    """
    if bid == 1:
        dfbook = dfbook[ dfbook['BIDSCUMVOL'] >= size]
        if not dfbook.empty:
            return dfbook.iloc[0]['BIDSVWAP']
    elif bid == 0:
        dfbook = dfbook[ dfbook['ASKSCUMVOL'] >= size ]
        if not dfbook.empty:
            return dfbook.iloc[0]['ASKSVWAP']
    return None 


def get_arb_portfolio_price(s, amount, books, bid = True):
    
    """
    portfolio is defined as long ritc and short others
    """
    last_Bear = vwap('BEAR', s, amount, books['bear'], 1 - int(bid))
    if last_Bear is None:
        return None
    last_Bull = vwap('BULL', s, amount, books['bull'], 1 - int(bid)) 
    if last_Bull is None:
        return None
    last_RITC = vwap('RITC', s, HEDGE_RATIO*amount,  books['ritc'], int(bid))
    if last_RITC is None:
        return None
    last_USD = om.sec_info(s, 'USD')[0]['ask']
    # price diff in etf which includes fees for execution market orders
    if bid:
        diff =   last_RITC*last_USD - (last_Bear + last_Bull) - 0.02*2 - 0.02*last_USD
    else:
        diff = last_RITC*last_USD - (last_Bear + last_Bull) - 0.02*2 - 0.02*last_USD

    return diff, [last_Bear, last_Bull, last_RITC, last_USD]


def submit_portfolio_order(action, qty, books, prices, session):
    if action == 'BUY':
        etf_action= 'BUY'
        stock_action = 'SELL'
        prices[2] = prices[2] + 0.01
        prices[1] = prices[1] - 0.01
        prices[0] = prices[0] - 0.01
    else:
        etf_action= 'SELL'
        stock_action = 'BUY'
        prices[2] = prices[2] - 0.01
        prices[1] = prices[1] + 0.01
        prices[0] = prices[0] + 0.01

#    om.bulk_submit({'ticker': 'RITC', 'type': 'LIMIT', 'quantity': qty['ritc'],
#                            'action': etf_action, 'price': prices[2]}, session)
#    # sell bear and bull
#    om.bulk_submit({'ticker': 'BULL', 'type': 'LIMIT', 'quantity': qty['bull'],
#                        'action': stock_action, 'price': prices[1]}, session)
#    om.bulk_submit({'ticker': 'BEAR', 'type': 'LIMIT', 'quantity':  qty['bear'],
#                        'action': stock_action, 'price': prices[0]}, session)
        
    om.bulk_submit({'ticker': 'RITC', 'type': 'MARKET', 'quantity': qty['ritc'],
                            'action': etf_action}, session)
    # sell bear and bull
    om.bulk_submit({'ticker': 'BULL', 'type': 'MARKET', 'quantity': qty['bull'],
                        'action': stock_action}, session)
    om.bulk_submit({'ticker': 'BEAR', 'type': 'MARKET', 'quantity':  qty['bear'],
                        'action': stock_action}, session)
    

def get_books(session):
    dfbear = om.get_order_book('BEAR', session, limit=20)
    dfbear = utils.parse_book(dfbear)
    dfbull = om.get_order_book('BULL', session, limit=20)
    dfbull = utils.parse_book(dfbull)
    dfritc = om.get_order_book('RITC', session, limit=20)
    dfritc = utils.parse_book(dfritc)
    books = {
            'bear': dfbear,
            'bull': dfbull,
            'ritc': dfritc
            }
    return books
    
def run(name, session):
    global POSITION
    global dfportfolio
    global SCALE_LEVELS
    # Use the maximum amount possible
    cfg = algo.config[name]
    exit_score = cfg['exit_score']
    dfsec = pd.DataFrame(om.sec_info(session))
    usd_pos = dfsec.loc[dfsec['ticker'] == 'USD', 'position'].values[0]
    # hedge currency
    if usd_pos < 0:
        om.bulk_submit({'ticker': 'USD', 'type': 'MARKET', 'quantity': abs(usd_pos),
                        'action': 'BUY'}, session)
    elif  usd_pos > 0:
        om.bulk_submit({'ticker': 'USD', 'type': 'MARKET', 'quantity': abs(usd_pos),
                        'action': 'SELL'}, session)
        
    books = get_books(session)
    dfbear = books['bear']
    dfbull = books['bull']
    dfritc = books['ritc']
    if dfbull.empty or dfritc.empty or dfbear.empty:
        return
    portfolio_bid_qty = min(
                  10000,
                  dfritc['BIDSCUMVOL'].iloc[-1],
                  dfbull['ASKSCUMVOL'].iloc[-1],
                  dfbear['ASKSCUMVOL'].iloc[-1],
                  )
    portfolio_ask_qty = min(
                  10000,
                  dfritc['ASKSCUMVOL'].iloc[-1],
                  dfbull['BIDSCUMVOL'].iloc[-1],
                  dfbear['BIDSCUMVOL'].iloc[-1],
                  )
    
    portfolio_bid, bid_prices = get_arb_portfolio_price(session, portfolio_bid_qty, books, True)
    if portfolio_bid is None:
        return 
    portfolio_ask, ask_prices = get_arb_portfolio_price(session, portfolio_ask_qty, books,  False)
    if portfolio_ask is None:
        return 
    if np.any(portfolio_bid > SCALE_LEVELS) and POSITION <= 0 and abs(POSITION)*3<240000:
        qty = {
            'ritc': HEDGE_RATIO*portfolio_bid_qty,
            'bull': portfolio_bid_qty,
            'bear': portfolio_bid_qty
            }
        submit_portfolio_order('SELL', qty, books, bid_prices, session)
        POSITION += -1*portfolio_bid_qty
        SCALE_LEVELS = SCALE_LEVELS[ portfolio_bid <= SCALE_LEVELS]
        
        print('GOING SHORT at price: ', portfolio_bid, POSITION)
#        time.sleep(2)
#        om.bulk_cancel(session)
        
    elif np.any(portfolio_ask < -1*SCALE_LEVELS) and POSITION >= 0 and abs(POSITION)*3<240000:
        # BUY etf
        qty = {
            'ritc': HEDGE_RATIO*portfolio_ask_qty,
            'bull': portfolio_ask_qty,
            'bear': portfolio_ask_qty
            }
        submit_portfolio_order('BUY', qty, books, ask_prices, session)
        POSITION += portfolio_ask_qty
        SCALE_LEVELS = SCALE_LEVELS[ portfolio_ask >= -1*SCALE_LEVELS]
        print('GOING long at price: ', portfolio_ask, POSITION)
#        time.sleep(2)
#        om.bulk_cancel(session)        
    bear_pos = dfsec.loc[dfsec['ticker'] == 'BEAR', 'position'].values[0]
    bull_pos = dfsec.loc[dfsec['ticker'] == 'BULL', 'position'].values[0]
    ritc_pos = dfsec.loc[dfsec['ticker'] == 'RITC', 'position'].values[0]
    qty = {
            'bear': abs(bear_pos),
            'bull': abs(bull_pos),
            'ritc': abs(ritc_pos)
            }
    portfolio_bid, bid_prices = get_arb_portfolio_price(session, abs(POSITION), books, True)
    if portfolio_bid is None:
        return 
    portfolio_ask, ask_prices = get_arb_portfolio_price(session, abs(POSITION), books,  False)
    if portfolio_ask <= -1*exit_score  and POSITION < 0:
        print('closing short at price: ', portfolio_ask) 
        submit_portfolio_order('BUY', qty, books, ask_prices, session)
        POSITION = 0
        SCALE_LEVELS = np.array(range(1, 11))/10
        
    elif portfolio_bid >= exit_score and  POSITION > 0:
        print('closing long at price: ', portfolio_bid)
        submit_portfolio_order('SELL', qty, books, bid_prices, session)
        POSITION = 0
        SCALE_LEVELS = np.array(range(1, 11))/10
        
