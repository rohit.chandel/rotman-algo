"""
For testing cointegration of etf arb and finding half life of mean reversion

"""

import numpy as np
import pandas as pd
import statsmodels.api as sm
from statsmodels.tsa.stattools import adfuller
import requests
import time
# algo modules
import algo
import algo.lib.utils as utils
import algo.strategy.etfarbitrage as etfarb
import algo.lib.ts as ts

cfg = algo.config
api_key = {'X-API-key': cfg['api_key']}
cfg = algo.config
diffs = []
dfportfolio = ts.make_ts(['BID', 'ASK'])

with requests.Session() as s:
    s.headers.update(api_key)
    session_manager = utils.SessionManager(__name__, s)
    i = 0
    while session_manager.is_running():
        books = etfarb.get_books(s)
        bid = etfarb.get_arb_portfolio_price(s, 10000, books, True)
        ask = etfarb.get_arb_portfolio_price(s, 10000, books, False)

        dfportfolio = ts.insert(dfportfolio, {
                  'BID': [bid],
                  'ASK': [ask],
                   ts.TIMESTAMP: [pd.Timestamp.now(tz='UTC')]
                })
        time.sleep(0.5)

dfportfolio_bid = dfportfolio[['BID']]
dfportfolio_ask = dfportfolio[['ASK']]

result = adfuller(dfportfolio_bid.values.reshape(1,-1)[0])
print('bid ADF Statistic: %f' % result[0])
print('bid p-value: %f' % result[1])

# estimate half life of reversion
mod = sm.OLS(dfportfolio_bid.diff()[1:], dfportfolio_bid.shift(1)[1:]).fit()

print('bid halflife: ', -np.log(2)/mod.params[0])

result = adfuller(dfportfolio_ask.values.reshape(1,-1)[0])
print('ask ADF Statistic: %f' % result[0])
print('ask p-value: %f' % result[1])

# estimate half life of reversion
mod = sm.OLS(dfportfolio_ask.diff()[1:], dfportfolio_ask.shift(1)[1:]).fit()

print('ask halflife: ', -np.log(2)/mod.params[0])


dfportfolio_mid = dfportfolio.mean(axis=1)

result = adfuller(dfportfolio_mid.values.reshape(1,-1)[0])
print('mid ADF Statistic: %f' % result[0])
print('mid p-value: %f' % result[1])

# estimate half life of reversion
mod = sm.OLS(dfportfolio_mid.diff()[1:], dfportfolio_mid.shift(1)[1:]).fit()

print('mid halflife: ', -np.log(2)/mod.params[0])

result = adfuller(dfportfolio_mid.values.reshape(1,-1)[0])
print('mid ADF Statistic: %f' % result[0])
print('mid p-value: %f' % result[1])

# estimate half life of reversion
mod = sm.OLS(dfportfolio_mid.diff()[1:], dfportfolio_mid.shift(1)[1:]).fit()

print('mid halflife: ', -np.log(2)/mod.params[0])
