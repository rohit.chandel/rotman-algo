# -*- coding: utf-8 -*-
"""
Created on Sun Jan 26 18:51:53 2020

@author: mfeadmin
"""
# core modules
import requests
import logging
from time import sleep
# algo modules
import algo
import algo.lib.utils as utils
import algo.lib.om as om
import algo.lib.mm as mm
import algo.lib.research as research
import numpy as np
# from algo.lib.error import AlgoError


score_mult_bid = 0
score_mult_ask = 0
score_mult_bid_bear = 0
score_mult_ask_bear = 0
score_mult_bid_bull = 0
score_mult_ask_bull = 0
score_imp = 0

def init():
    global score_mult_bid
    global score_mult_ask
    global score_mult_bid_bear
    global score_mult_ask_bear
    global score_mult_bid_bull
    global score_mult_ask_bull
    global score_imp
    score_mult_bid = 0
    score_mult_ask = 0
    score_mult_bid_bear = 0
    score_mult_ask_bear = 0
    score_mult_bid_bull = 0
    score_mult_ask_bull = 0
    score_imp = 0
    
    
def run(name, s, trade_amount=5000, penalty=0.25, bull_spread=0.01,bear_spread=0.05):
    global score_mult_bid
    global score_mult_ask
    global score_mult_bid_bear
    global score_mult_ask_bear
    global score_mult_bid_bull
    global score_mult_ask_bull
    global score_imp
#    cfg = algo.config
#    strategy_params = cfg[name]
    #book_m = om.get_order_book('ALGO', s) #Get LOB for ALGO

    #if not book_m['bids'] or not  book_m['asks']:
     #   continue
    """
    Step 1: Get Orders
    """

#    BULL_orders = mm.get_orders(s,'BULL')
#    BEAR_orders = mm.get_orders(s,'BEAR')
    RITC_orders = mm.get_orders(s,'RITC')
    
    """
    Step 2: Reset bid-ask if nothing executed
    """
    
    bid_thresh = 0.05*(1 + score_mult_bid)
    ask_thresh= 0.05*(1 + score_mult_ask)

#    bid_thresh_bull = 0.01*(1 + score_mult_bid_bull)
#    ask_thresh_bull= 0.01*(1 + score_mult_ask_bull)
#
#    bid_thresh_bear = 0.025*(1 + score_mult_bid_bear)
#    ask_thresh_bear= 0.025*(1 + score_mult_ask_bear)
    #mm.convert_all_usd(s)

    """
    Step 2: Cancel One Sided
    """
    
    if len(RITC_orders) != 2:
        score = mm.cancel_enter_hedge_RITC(s,RITC_orders,trade_amount)
        if score == 'BUY':
            score_mult_bid = max(0,score_mult_bid + penalty)
            score_mult_ask = max(0,score_mult_ask - penalty)
            score_imp =1

        elif score == 'SELL':
            score_mult_ask = max(0,score_mult_ask + penalty)
            score_mult_bid = max(0,score_mult_bid - penalty)
            score_imp =1
            
        mm.limit_enter_RITC(s,"RITC",trade_amount,bid_thresh,ask_thresh)
    

    
#def run_mm(name,bull_spread=0.01,bear_spread=0.05):
#    cfg = algo.config
#    api_key = {'X-API-key': cfg['api_key']}
#    strategy_params = cfg[name]
#    with requests.Session() as s:
#        s.headers.update(api_key)
#        sm = utils.SessionManager(__name__, s)
#        
#        while sm.is_running():
#            #book_m = om.get_order_book('ALGO', s) #Get LOB for ALGO
#
#            #if not book_m['bids'] or not  book_m['asks']:
#             #   continue
#            """
#            Step 1: Get Orders
#            """
#
#            BULL_orders = mm.get_orders(s,'BULL')
#            BEAR_orders = mm.get_orders(s,'BEAR')
#            RITC_orders = mm.get_orders(s,'RITC')
#            
#            """
#            Step 2: Reset bid-ask if nothing executed
#            """
#            
#            bid_thresh = 0.05*(1 + score_mult_bid)
#            ask_thresh= 0.05*(1 + score_mult_ask)
#
#            bid_thresh_bull = 0.01*(1 + score_mult_bid_bull)
#            ask_thresh_bull= 0.01*(1 + score_mult_ask_bull)
#
#            bid_thresh_bear = 0.025*(1 + score_mult_bid_bear)
#            ask_thresh_bear= 0.025*(1 + score_mult_ask_bear)
#            #mm.convert_all_usd(s)
#
#            """
#            Step 2: Cancel One Sided
#            """
#            
#            if len(RITC_orders) != 2:
#
#
#                score = mm.cancel_enter_hedge_RITC(s,RITC_orders,5000)
#                if score == 'BUY':
#                    score_mult_bid = max(0,score_mult_bid + 0.25)
#                    score_mult_ask = max(0,score_mult_ask - 0.25)
#                    score_imp =1
#
#                elif score == 'SELL':
#                    score_mult_ask = max(0,score_mult_ask + 0.25)
#                    score_mult_bid = max(0,score_mult_bid - 0.25)
#                    score_imp =1
#                    
#                mm.limit_enter_RITC(s,"RITC",5000,bid_thresh,ask_thresh)

            # if len(BEAR_orders) != 2:

            #     score = mm.cancel_enter_hedge(s,BEAR_orders,2500)
            #     if score == 'BUY':
            #         score_mult_bid_bear = max(0,score_mult_bid_bear + 0.1)
            #         score_mult_ask_bear  = max(0,score_mult_ask_bear - 0.1)
            #         score_imp =1

            #     elif score == 'SELL':
            #         score_mult_ask_bear = max(0,score_mult_ask_bear + 0.1)
            #         score_mult_bid_bear = max(0,score_mult_bid_bear - 0.1)
            #         score_imp =1
                    
            #     mm.limit_enter(s,"BEAR",2500,bid_thresh_bear,ask_thresh_bear)

            # if len(BULL_orders) != 2:

            #     score = mm.cancel_enter_hedge(s,BULL_orders,2500)
            #     if score == 'BUY':
            #         score_mult_bid_bull = max(0,score_mult_bid_bull + 0.1)
            #         score_mult_ask_bull = max(0,score_mult_ask_bull - 0.1)
            #         score_imp =1

            #     elif score == 'SELL':
            #         score_mult_ask_bull = max(0,score_mult_ask_bull + 0.1)
            #         score_mult_bid_bull = max(0,score_mult_bid_bull - 0.1)
            #         score_imp =1
                    
            #     mm.limit_enter(s,"BULL",2500,bid_thresh_bull,ask_thresh_bull)

                # mm.limit_enter(s,"RITC",10000,0.1,0.1,use_last = True)
                # mm.limit_enter(s,"RITC",10000,0.15,0.15,use_last = True)

            # if len(BEAR_orders) != 2:
            #     #mm.cancel_enter_RITC(s,BEAR_orders,1000)
            #     mm.limit_enter(s,"RITC",10000,0.1,0.1,use_last = True )
            #     mm.limit_enter(s,"RITC",10000,0.15,0.15,use_last = True)
            #     mm.limit_enter(s,"RITC",10000,0.25,0.25,use_last = True)

# =============================================================================
#             last_bear = om.sec_info('BEAR',s)[0]['last'] #last price
#             #set spread for orders
#             bid_lmt_bear = last_bear - bear_spread
#             ask_lmt_bear = last_bear + bear_spread
#
#             last_bull = om.sec_info('BULL',s)[0]['last'] #last price
#             #set spread for orders
#             bid_lmt_bull = last_bull - bull_spread
#             ask_lmt_bull = last_bull + bull_spread
#
#             """
#             Step 2: Get live orders
#             """
#             orders = om.get_orders(s)
#
#             """
#             Step 3: Cancel one-sided orders
#             """
#             if len(orders) != 4: #dumbass
#                 om.cancel_all_orders(s)
#
#             """
#             Step 4: Submit Bid/Ask Limit orders if no orders live
#             """
#             if len(orders) == 0:
#                 msg_buy  = {'ticker': 'BULL', 'type': 'LIMIT', 'quantity': 5000,
#                             'action': 'BUY', "price": bid_lmt_bull}
#                 msg_sell  = {'ticker': 'BULL', 'type': 'LIMIT', 'quantity': 5000,
#                              'action': 'SELL', "price": ask_lmt_bull}
#                 om.submit(msg_buy,s)
#                 om.submit(msg_sell,s)
#
#                 msg_buy  = {'ticker': 'BEAR', 'type': 'LIMIT', 'quantity': 5000,
#                             'action': 'BUY', "price": bid_lmt_bear}
#                 msg_sell  = {'ticker': 'BEAR', 'type': 'LIMIT', 'quantity': 5000,
#                              'action': 'SELL', "price": ask_lmt_bear}
#                 om.submit(msg_buy,s)
#                 om.submit(msg_sell,s)
#             #can't post too frequently
# =============================================================================
#            sleep(strategy_params['sleep_interval'])
#
#    logging.getLogger(name).info('Strategy done.')

#def run_research(name,bull_spread=0.01,bear_spread=0.05):
#    cfg = algo.config
#    api_key = {'X-API-key': cfg['api_key']}
#    strategy_params = cfg[name]
#    with requests.Session() as s:
#        s.headers.update(api_key)
#        sm = utils.SessionManager(__name__, s)
#        while sm.is_running():
#            print(om.get_tick(s))
#            print(research.TAS(s,"RITC",1))





if __name__ == '__main__':
    run_mm('algo.strategy.algo_mm')