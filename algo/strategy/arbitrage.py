"""
Simple arbitrage strategy

"""
# core modules
import requests
import logging
from time import sleep
# algo modules
import algo
import algo.lib.utils as utils
import algo.lib.om as om
# from algo.lib.error import AlgoError

requests.adapters.DEFAULT_RETRIES = 5

def run(name):
    cfg = algo.config
    api_key = {'X-API-key': cfg['api_key']}
    strategy_params = cfg[name]
    with requests.Session() as s:
        s.headers.update(api_key)
        sm = utils.SessionManager(__name__, s)
        while sm.is_running():
            book_m = om.get_order_book('CRZY_M', s)
            book_a = om.get_order_book('CRZY_A', s)
            if not book_m['bids'] or not  book_m['asks']:
                continue
            best_bid_m = book_m['bids'][0]
            best_ask_m = book_m['asks'][0]
            best_bid_a = book_a['bids'][0]
            best_ask_a = book_a['asks'][0]
            
            # arbitrate conditions
            if best_ask_a['price'] < best_bid_m['price']:
                qty = min(best_ask_a['quantity'], best_bid_m['quantity'], 10000)
                msg  = {'ticker': 'CRZY_A', 'type': 'MARKET', 'quantity': qty,
                        'action': 'BUY'}
                status = om.submit(msg, s)
                if status:
                    msg  = {'ticker': 'CRZY_M', 'type': 'MARKET', 
                        'quantity': qty, 'action': 'SELL'}
                    #om.submit(msg, s)
            if best_ask_m['price'] < best_bid_a['price']:

                qty = min(best_ask_m['quantity'], best_bid_a['quantity'], 10000)
                msg  = {'ticker': 'CRZY_M', 'type': 'MARKET', 'quantity': qty,
                        'action': 'BUY'}
                status = om.submit(msg, s)
                if status:
                    msg  = {'ticker': 'CRZY_A', 'type': 'MARKET', 
                        'quantity': qty, 'action': 'SELL'}
                    #Som.submit(msg, s)
            sleep(strategy_params['sleep_interval'])
    logging.getLogger(name).info('Strategy done.')
                
            
if __name__ == '__main__':
    run('algo.strategy.arbitrage')