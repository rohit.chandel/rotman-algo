"""
etf arbitrage
"""
# core modules
import logging
import pandas as pd
# algo modules
import algo.lib.utils as utils
import algo.lib.om as om
import numpy as np
import time

profit = 0
unfilled_qty = 0

def vwap(dfbook, size, bid = 1):
    """
    0 for bid 1 for ask
    """
    if bid == 1:
        dfbook = dfbook[ dfbook['BIDSCUMVOL'] >= size]
        if not dfbook.empty:
            return dfbook.iloc[0]['BIDSVWAP']
    elif bid == 0:
        dfbook = dfbook[ dfbook['ASKSCUMVOL'] >= size ]
        if not dfbook.empty:
            return dfbook.iloc[0]['ASKSVWAP']
    return None 

def submit_portfolio_order(action, qty, prices, session):
    global profit, unfilled_qty
    if action == 'BUY':
        etf_action= 'BUY'
        stock_action = 'SELL'
    else:
        etf_action= 'SELL'
        stock_action = 'BUY'

    ack1 = om.bulk_submit({'ticker': 'RITC', 'type': 'MARKET', 'quantity': qty,
                            'action': etf_action}, session)
    # sell bear and bull
    ack2 = om.bulk_submit({'ticker': 'BULL', 'type': 'MARKET', 'quantity': qty,
                        'action': stock_action}, session)
    ack3= om.bulk_submit({'ticker': 'BEAR', 'type': 'MARKET', 'quantity':  qty,
                        'action': stock_action}, session)
    time.sleep(1)
    if etf_action == 'BUY':
        usd_qty = ack1['vwap']*qty + .15*qty
    else:
        usd_qty = ack1['vwap']*qty - .15*qty
    ack4 = om.submit({'ticker': 'USD', 'type': 'MARKET', 'quantity': usd_qty,
                        'action': etf_action}, session)
    print('expected price:', prices['bull'], prices['bear'], prices['ritc'], ack4['vwap'])
    print('gotten price:', ack2['vwap'], ack3['vwap'], ack1['vwap'])
    profit += ((ack1['vwap']*ack4['vwap']  - ack3['vwap'] - ack2['vwap']) -  0.02*2 - 0.02*ack4['vwap']  - 0.15*ack4['vwap'])*qty
    unfilled_qty += (qty - min(ack1['quantity_filled'], ack2['quantity_filled'], ack3['quantity_filled'])) 
    print('realized profit: ', profit, ' unfilled qty: ', unfilled_qty)
    

def init():
    global profit, unfilled_qty
    profit = 0
    unfilled_qty = 0

    
def run(name, session):
    thresh = 0.2
    dfbear = om.get_order_book('BEAR', session, limit=10)
    dfbear = utils.parse_book(dfbear)
    dfbull = om.get_order_book('BULL', session, limit=10)
    dfbull = utils.parse_book(dfbull)
    dfritc = om.get_order_book('RITC', session, limit=10)
    dfritc = utils.parse_book(dfritc)
    usdbook = om.sec_info(session, 'USD')[0]
    
    portfolio_bid_qty = min(
                  10000,
                  dfritc['BIDSCUMVOL'].iloc[2],
                  dfbull['ASKSCUMVOL'].iloc[2],
                  dfbear['ASKSCUMVOL'].iloc[2]
                  )
    portfolio_ask_qty = min(
                  10000,
                  dfritc['ASKSCUMVOL'].iloc[2],
                  dfbull['BIDSCUMVOL'].iloc[2],
                  dfbear['BIDSCUMVOL'].iloc[2]
                  )
#    usdask = usdbook['ask']
    usdbid = usdbook['bid']
    ritcbid = vwap(dfritc, portfolio_bid_qty, bid=1) 
    ritcask = vwap(dfritc, portfolio_ask_qty, bid=0)
    
    bearbid = vwap(dfbear, portfolio_ask_qty, bid=1)
    bearask = vwap(dfbear, portfolio_bid_qty, bid=0)
    
    bullbid = vwap(dfbull, portfolio_ask_qty, bid=1)
    bullask = vwap(dfbull, portfolio_bid_qty, bid=0)
    
    tcost = 1500/10000
    ritcbid = dfritc['BIDS'].iloc[0]
    ritcask = dfritc['ASKS'].iloc[0]
    
    bearbid = dfbear['BIDS'].iloc[0]
    bearask = dfbear['ASKS'].iloc[0]
    
    bullbid = dfbull['BIDS'].iloc[0]
    bullask = dfbull['ASKS'].iloc[0]

#    last_USD = om.sec_info_k('USD', session)[0]['last']
    long_profit = ritcask*usdbid - bearbid - bullbid - 0.02*2 - 0.02*usdbid - tcost*usdbid
    short_profit = (ritcbid*usdbid  - bearask - bullask) -  0.02*2 - 0.02*usdbid - tcost*usdbid
    dfsec = pd.DataFrame(om.sec_info(session))
    ritc_pos = dfsec.loc[dfsec['ticker'] == 'RITC', 'position'].values[0]
    max_profit = max(long_profit, short_profit)
#    if max_profit == short_profit and short_profit > thresh   and abs(ritc_pos) < 5000 and abs(ritc_pos)*3 < 299000:
#        prices = {
#                    'bull': bullask,
#                    'bear': bearask,
#                    'ritc': ritcbid
#                    }
#        submit_portfolio_order('SELL', portfolio_bid_qty, prices, session)
#        print('GOING SHORT at price: ', short_profit, ritc_pos)
        
    if max_profit == long_profit and long_profit > thresh  and abs(ritc_pos) < 5000 and abs(ritc_pos)*3 < 299000:
        prices = {
                    'bull': bullbid,
                    'bear': bearbid,
                    'ritc': ritcask
                    }
        submit_portfolio_order('BUY', portfolio_ask_qty, prices, session)
        print('GOING LONG at price: ', long_profit, ritc_pos)




            
        
        
        
    
