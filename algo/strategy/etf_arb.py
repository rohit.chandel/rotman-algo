# -*- coding: utf-8 -*-
"""

ETF ARBITRAGE FOR ROTMAN 2020 ALGO TRADING 


Created on Tue Feb  4 12:21:00 2020

@author: kevin
"""
import requests
import logging
# algo modules
from time import sleep
import algo
import algo.lib.utils as utils
import algo.lib.om as om
import pandas as pd
import numpy as np

def vwap(ticker,s,size,side = 0):
    """
    0 for bid 1 for ask
    """
    ob = om.get_order_book(ticker,s)
    dfbook = utils.parse_book(ob)
    if side == 0:
        dfbook = dfbook[ dfbook['BIDSCUMVOL'] >= size]
        if not dfbook.empty:
            return dfbook.iloc[0]['BIDSVWAP']
    elif side == 1:
        dfbook = dfbook[ dfbook['ASKSCUMVOL'] >= size ]
        if not dfbook.empty:
            return dfbook.iloc[0]['ASKSVWAP']
    return None 

def ETF_diff(s,amount, buy_in = True):
    """
    #TODO Rework for buying or selling 
    P_ritc_theo = (p_Bear + p_Bull)/USD
    returns P_RITC - p_ritc_theo, difference in actual RITC and fair value    
    """

    if buy_in:
        last_Bear = vwap('BEAR',s,amount,1)  #ask
        last_Bull = vwap('BULL',s,amount,1) #ask
        last_USD = om.sec_info_k('USD',s)[0]['bid']
        last_RITC = vwap('RITC',s,amount,0)
        
        prices = [last_RITC,last_USD,last_Bull,last_Bear]
        return last_RITC*last_USD - (last_Bear + last_Bull) - 0.02*2 - 0.02*last_USD, prices
        #returns price diff in etf which includes fees for execution market orders 
    
    else:
        last_Bear = vwap('BEAR',s,amount,0) 
        last_Bull = vwap('BULL',s,amount,0)
        last_USD = om.sec_info_k('USD',s)[0]['ask']
        last_RITC = vwap('RITC',s,amount,1)
                
        prices = [last_RITC,last_USD,last_Bull,last_Bear]
        return -(last_RITC*last_USD - (last_Bear + last_Bull)) - 0.02*2 - 0.02*last_USD, prices
       

def position_in_out(x,closer = False):
    if closer:
        x = -x
    if x > 0:
        return 'BUY'
    elif x < 0:
        return 'SELL'
    else:
        return None
        
    
def position_close_out(s,amount):
    '''
    Closes out position according to ETF arb strat
    position should not be more than 10k
    '''
    etf_position = om.sec_info_k('RITC',s)[0]['position']
    bull_position = om.sec_info_k('BULL',s)[0]['position']
    bear_position = om.sec_info_k('BEAR',s)[0]['position']
   
    etf_dir = position_in_out(etf_position,True)
    bull_dir = position_in_out(bull_position,True)
    bear_dir = position_in_out(bear_position,True)

    if etf_dir != None:
        last_usd = om.sec_info_k('USD',s)[0]['last']
        #close out RITC 
        msg_RITC =  {'ticker': 'RITC', 'type': 'MARKET', 'quantity': amount,'action': etf_dir}
        om.submit(msg_RITC,s)
        #Covert USD to CAD or vice versa depending on RITC trade 
        msg_USD =  {'ticker': 'USD', 'type': 'MARKET', 'quantity': amount*last_usd,'action': etf_dir}
        om.submit(msg_USD,s)        

    if bull_dir != None:
        #close out bull
        msg_Bull =  {'ticker': 'BULL', 'type': 'MARKET', 'quantity': amount,'action': bull_dir}
        om.submit(msg_Bull,s)

    if bear_dir != None :   
        msg_Bear =  {'ticker': 'BEAR', 'type': 'MARKET', 'quantity': amount,'action': bear_dir}
        om.submit(msg_Bear,s)

    return None   


def arb_enter(s,amount,etf_pos,stock_pos,prices,direction):
    vwap_price = prices[0]
    msg_RITC =  {'ticker': 'RITC', 'type': 'MARKET', 'quantity': amount,
                            'action': etf_pos}
    msg_Bull =  {'ticker': 'BULL', 'type': 'MARKET', 'quantity': amount,
                            'action': stock_pos}
    msg_Bear =  {'ticker': 'BEAR', 'type': 'MARKET', 'quantity': amount,
                            'action': stock_pos}
    om.submit(msg_RITC,s)
    om.submit(msg_Bull,s)
    om.submit(msg_Bear,s)

    
    #upper bound 
    # msg_RITC =  {'ticker': 'RITC', 'type': 'LIMIT', 'quantity': amount,
    #                         'action': stock_pos, 'price': vwap_price + direction*0.1}
    
    
    # om.submit(msg_RITC,s) 

    # msg_RITC =  {'ticker': 'RITC', 'type': 'LIMIT', 'quantity': amount,
    #                         'action': stock_pos, 'price': vwap_price + direction*0.05}
    
    # om.submit(msg_RITC,s) 



def ETF_arb(s,full_amount = 10000, amount = 10000, profit_threshold = 0.3):
    """
    if ETF_diff_in is greater than profit_threshold, short ETF, long stocks
    if ETF_diff_out greater than profit_threshold, long ETF and short stocks 
    """
    position = om.sec_info_k('RITC',s)[0]['position']    #ritc position 
    #----------------------------------------------------
    # Step 1: Check for arbitrage opportunity
    #----------------------------------------------------
    etf_diff_in, prices_in = ETF_diff(s,full_amount)
    etf_diff_out,prices_out = ETF_diff(s,full_amount,False)

    #----------------------------------------------------
    # Step 2: If arbitrage, set up directions for buy/sell
    #-----------------------------------------------------

    if etf_diff_in > profit_threshold:
        etf_pos = 'SELL' 
        stock_pos = 'BUY'
        direction = -1
        prices = prices_in
    elif etf_diff_out > profit_threshold:
        etf_pos = 'BUY' 
        stock_pos = 'SELL'
        direction = 1
        prices = prices_out
    #figure this one out

    #----------------------------------------------------
    # Step 3: If strategy is live, and spread has shrunk, then close positions
    #---------------------------------------------------
    else:
        if position != 0: #is strategy live? 
            if ((position < 0) and (etf_diff_out > 0)) or ((position > 0) and (etf_diff_in > 0)): #is it within threshold? 
                count = 0
                while count < full_amount/amount:
                    position_close_out(s,amount)
                    sleep(5)
                    count += 1    
        return None  

    #-----------------------------------------------------
    # Step 4: Enter positions or keep positions is strategy activated 
    #-----------------------------------------------------    

    if position == direction*full_amount: # Means that strategy is live, and spread not captured yet 
        return None 

    elif position == 0: # Means strategy is not live, need to enter 
        om.cancel_all_orders(s)
        count = 0
        while count < (full_amount-np.abs(position))/amount:
            arb_enter(s,amount,etf_pos,stock_pos,prices,direction) #will scale up accordingly 
            count += 1


        usd_amt = full_amount*prices[0] #full USD amt
        msg_USD =  {'ticker': 'USD', 'type': 'MARKET', 'quantity': usd_amt,
                            'action': etf_pos}
        om.submit(msg_USD,s)
        sleep(8)
        count = 0
        while count < full_amount/amount:
            position_close_out(s,amount)
            sleep(5)
            count += 1
        
        
        

        
        return None










"""-----------------------Redemeption Creation------------"""

def redemption_value(s):
    """
    Value of converting then liquidating underlying positions
    Defaults to a 10k liquidation 
    """
    book_Bear = om.get_order_book('BEAR', s, limit=50)
    book_Bear = utils.parse_book(book_Bear)
    book_Bull = om.get_order_book('BULL', s, limit=50)
    book_Bull = utils.parse_book(book_Bull)
    last_USD = om.sec_info_k('USD',s)[0]['last']
    cost = 1500*last_USD
    book_Bear = book_Bear[ book_Bear['BIDSCUMVOL'] >= 10000]
    book_Bull = book_Bull[ book_Bull['BIDSCUMVOL'] >= 10000]
    total_value = 10000*(book_Bear.iloc[0]['BIDSVWAP'] + book_Bull.iloc[0]['BIDSVWAP']) - cost 
    return total_value

def creation_value(s):
    """
    Value of creating RITC then liquidating positions
    Defaults to a 20k liquidation 
    """
    book_RITC = om.get_order_book('RITC', s, limit=50)
    book_RITC = utils.parse_book(book_RITC)    
    last_USD = om.sec_info_k('USD',s)[0]['last']
    cost = 1500*last_USD
    book_RITC = book_RITC[ book_RITC['BIDSCUMVOL'] >= 20000]
    total_value = 10000*(book_RITC.iloc[0]['BIDSVWAP']) - cost 
    return total_value

def run_etf():
    cfg = algo.config
    api_key = {'X-API-key': cfg['api_key']}
    with requests.Session() as s:
        s.headers.update(api_key)
        sm = utils.SessionManager(__name__, s)
        
        while sm.is_running():
            ETF_arb(s,40000,5000,0.1)
            

if __name__ == '__main__':
    run_etf()
    
##############3 OLDER IDEAS 


# def ETF_arb_entry(s,profit_threshold = 0.3):
#     """
#     TODO: Rework the logic!!
#     if ETF_diff is pos, short ETF, long 
#     if ETF_diff is neg, expect USD to gain
#     short/long based on this information 
    
#     Turns out not to be that good of a strategy lol 
#     Try Entering USD position together with RITC 
#     """
#     #step 0 Parameters
#     amount = 5000  
#     #step 1 check direction
#     etf_diff_in, prices_in = ETF_diff(s)
#     etf_diff_out,prices_out = ETF_diff(s,False)
    
#     if etf_diff_in > profit_threshold:
#         """
#         We want to sell the ETF, buy the stock position
#         """
#         etf_pos = 'SELL' 
#         stock_pos = 'BUY'
#         direction = -1

#     elif etf_diff_out > profit_threshold:
#         """
#         We want to buy the ETF, sell the stock position
#         """
#         etf_pos = 'BUY' 
#         stock_pos = 'SELL'
#         direction = 1

#     else:
#         #do nothing 
#         return None    


#     """
#     Check if strategy already live

#     If live - will have some position in any of RITC, Bull, or Bear
#     If so, do nothing 
#     """
#     if (om.sec_info('RITC',s)[0]['position'] != 0) or (om.sec_info('BULL',s)[0]['position'] !=0) or (om.sec_info('BEAR',s)[0]['position'] != 0):
#         return None

#     """
#     Enter Market Orders
#     """
#     usd_amt = amount*prices_in[1]

#     RITC_ = {'ticker': 'RITC', 'type': 'MARKET', 'quantity': amount,
#                         'action': etf_pos}
#     BULL_ = {'ticker': 'BULL', 'type': 'MARKET', 'quantity': amount,
#                         'action': stock_pos}
#     BEAR_ = {'ticker': 'BEAR', 'type': 'MARKET', 'quantity': amount,
#                         'action': stock_pos}

#     om.submit(RITC_,s)
#     om.submit(BULL_,s)
#     om.submit(BEAR_,s)

#     """
#     Currency Hedge
#     """
#     USD_ = {'ticker': 'USD', 'type': 'MARKET', 'quantity': usd_amt,
#                         'action': etf_pos}
#     om.submit(USD_,s)

#     """
#     Enter Limit Orders
#     """

#     #get lmt prices
#     if direction == 1: #we bought RITC 
#         RITC_price = prices_out[0] + profit_threshold*(2/3)
#         Bull_price = prices_out[2] - profit_threshold*(2/3)
#         Bear_price = prices_out[3] - profit_threshold*(2/3)
#     else:
#         RITC_price = prices_in[0] - profit_threshold*(2/3)
#         Bull_price = prices_in[2] + profit_threshold*(2/3)
#         Bear_price = prices_in[3] + profit_threshold*(2/3)        

#     RITC_lmt = {'ticker': 'RITC', 'type': 'LIMIT', 'quantity': amount,
#                         'action': stock_pos, "price": RITC_price} #reverse position
#     BULL_lmt = {'ticker': 'BULL', 'type': 'LIMIT', 'quantity': amount,
#                         'action': etf_pos, "price": Bull_price}
#     BEAR_lmt = {'ticker': 'BEAR', 'type': 'LIMIT', 'quantity': amount,
#                         'action': etf_pos, "price": Bear_price}    

#     om.submit(RITC_lmt,s)
#     om.submit(BULL_lmt,s)
#     om.submit(BEAR_lmt,s)


#     return None 

# def ETF_arb_manager(s):
#     """
#     Check how many live limit orders (should be 3 at maximum)
#     """
#     orders = om.get_orders(s)

#     """
#     If less than max (3), then either limits were filled or strategy not live
#     Either way, cancel all live orders & close out position
#     """
#     if len(orders) < 3:
#         for order in orders:
#             om.cancel(order['order_id'],s) #cancel remaining orders 
#         #now close out position
#         position_close_out(s) #close out inventory 
#         sleep(0.5)
#         #now enter strategy
#         ETF_arb_entry(s) #restart the strategy 
#     return None 





    # #step 2 check position    
    # position = om.sec_info('RITC',s)[0]['position']    
    # #either change direction or do nothing if in the right direction
    # if position == direction*amount:
    #     #print('position_correct')
    #     return None
    
    # elif position == 0:
    #     amount_list = amount_list
    #     usd_amt = -amount_list[0]*prices[0] #USD hedge?
        
    # elif position == -1*direction*amount:
    #     amount_list = amount_list*2
    #     usd_amt = -amount_list[0]*prices[0]
    #     print('reversing position')
    
    # msg_RITC =  {'ticker': 'RITC', 'type': 'MARKET', 'quantity': amount_list[0],
    #                     'action': etf_pos}
    # msg_Bull =  {'ticker': 'BULL', 'type': 'MARKET', 'quantity': amount_list[1],
    #                     'action': stock_pos}
    # msg_Bear =  {'ticker': 'BEAR', 'type': 'MARKET', 'quantity': amount_list[2],
    #                     'action': stock_pos}
    # msg_USD =  {'ticker': 'USD', 'type': 'MARKET', 'quantity': usd_amt,
    #                     'action': etf_pos}
    # om.submit(msg_RITC,s)
    # om.submit(msg_Bull,s)
    # om.submit(msg_Bear,s)
    # om.submit(msg_USD,s)
    # print('taking position')
    # return None
             
