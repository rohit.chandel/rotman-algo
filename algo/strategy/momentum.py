"""
ETF arbitrage using cointegration
"""
import logging
import algo.lib.utils as utils
import algo.lib.om as om
import pandas as pd
import algo.lib.ts as ts
import algo
import numpy as np

class MomBot:
        
    def __init__(self):
        self.dfmid = ts.make_ts(['MID'])
        self.POSITION = 0
        
    def run(self, name, ticker, amount, session):
        lookback = 50
        thresh = 0.015/20
        # 1%
        book = om.get_order_book(ticker, session)
        dfbook = utils.parse_book(book)
        dfbook = dfbook[ (dfbook['BIDSCUMVOL'] >= amount) & (dfbook['ASKSCUMVOL'] >= amount)]
        if dfbook.empty:
            logging.getLogger(name).info('not enough liquidity')
            return
        self.dfmid = ts.insert(self.dfmid, {
                      'MID':[(dfbook['ASKS'].values[0] + dfbook['BIDS'].values[0])/2],
                       ts.TIMESTAMP: [pd.Timestamp.now(tz='UTC')]
                    })
        
        dfret = (self.dfmid/self.dfmid.shift(1)) - 1
        if len(dfret) < lookback:
            return
#         TODO: try exponentially weighted mean
        momentum_sig = dfret.rolling(window=lookback).mean().iloc[-1].values[0]
        print('sig: ', momentum_sig, thresh)
#        
#        dfhistory = om.get_trade_history(ticker, session)
#        if dfhistory is None:
#            return
#        marktet_status = session.get(cfg['client']+'/case').json()
#        if marktet_status.get('status', 'STOPPED') !='ACTIVE':
#            return
#        tick = marktet_status['tick']
#    
#        dfhistory.index = dfhistory['tick']
#        dfhistory = dfhistory.reindex(range(0, tick+1), method='ffill')
#        
#        dfhistory['ret'] = dfhistory['vwap']/ dfhistory['vwap'].shift(1) - 1
#        dfhistory['ret_vol'] =  dfhistory['ret']*dfhistory['quantity']
#        if len(dfhistory) < lookback_long:
#            return
#        # volume weighted returns
#        momentum_sig_long = np.sum(dfhistory['ret_vol'].iloc[-lookback_long:])/np.sum(dfhistory['quantity'].iloc[-lookback_long:])
#        momentum_sig_short = np.sum(dfhistory['ret_vol'].iloc[-lookback_short:])/np.sum(dfhistory['quantity'].iloc[-lookback_short:])

#
        if momentum_sig >  thresh and self.POSITION == 0:
            # buy
            print('BUY', momentum_sig, thresh, self.POSITION)
            om.bulk_submit({'ticker': ticker, 'type': 'MARKET', 'quantity': amount,
                                'action': 'BUY'}, session)
            self.POSITION = amount
       
        if momentum_sig < -thresh and self.POSITION == 0:
            #sell
            print('SELL', momentum_sig, momentum_sig, self.POSITION)
            om.bulk_submit({'ticker': ticker, 'type': 'MARKET', 'quantity': amount,
                                'action': 'SELL'}, session)
            self.POSITION = -amount
        
        if momentum_sig > 0 and self.POSITION < 0:
            # buy
            print('CLOSE OUT SHORT', momentum_sig, self.POSITION)
            om.bulk_submit({'ticker': ticker, 'type': 'MARKET', 'quantity': amount,
                                'action': 'BUY'}, session)
            self.POSITION = 0
        if momentum_sig < 0 and self.POSITION > 0:
            #sell
            print('CLOSE OUT LONG', momentum_sig, self.POSITION)
            om.bulk_submit({'ticker': ticker, 'type': 'MARKET', 'quantity': amount,
                                'action': 'SELL'}, session)
            self.POSITION = 0
    
    