"""
currency arbitrage
"""
# core modules
import logging
import pandas as pd
# algo modules
import algo
import algo.lib.utils as utils
import algo.lib.om as om
import time



def run(name, session):
    lookback = 25
    scale_levels = [2, 3, 4, 5, 6]
