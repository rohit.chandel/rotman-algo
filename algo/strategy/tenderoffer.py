"""
tender offer arbitrage
"""
# core modules
import requests
import logging
import pandas as pd
# algo modules
import algo
import algo.lib.utils as utils
import algo.lib.om as om
import time
# from algo.lib.error import AlgoError

requests.adapters.DEFAULT_RETRIES = 5

def execute_tender(offer, dfbook, strategy_params, session):
    """
    helper func to decide whether to accept offer
    """
    # TODO: add support to non fixed bid tenders
    ticker = offer['ticker']
    market_fee = algo.config['market_fee']
    if offer['action'] == 'BUY':
        book_prefix = 'BIDS'
        close_dir ='SELL'
        offer_val = offer['price'] + market_fee + strategy_params['profit_threshold']
    else:
        book_prefix = 'ASKS'
        close_dir ='BUY'
        offer_val = offer['price'] - market_fee - strategy_params['profit_threshold']
    cumvol_col = book_prefix + 'CUMVOL'
    vwap_col = book_prefix + 'VWAP'
    
    dfbook = dfbook[dfbook[cumvol_col] >= offer['quantity']]
    
    if not dfbook.empty and offer['action'] == 'BUY':
        market_val =   dfbook.iloc[0][vwap_col]
        trade_condition = offer_val < market_val
    elif not dfbook.empty and offer['action'] == 'SELL':
        market_val =   dfbook.iloc[0][vwap_col]
        trade_condition = offer_val > market_val
    else:
        trade_condition = False
    
    if trade_condition:
        # accept
        successful = om.accept_tender(offer['tender_id'], session)
        if successful:
            # comment this for aggressive
            order_size = offer['quantity']
            om.bulk_submit({
                    'ticker': ticker, 'type': 'MARKET',
                    'quantity':  order_size,
                    'action': close_dir}, session)
            time.sleep(0.5)
            # -----
            if ticker == 'RITC':
                dfsec = pd.DataFrame(om.sec_info(session))
                position = dfsec.loc[dfsec['ticker'] == 'USD', 'position'].values[0]
                if position > 0:
                    om.bulk_submit({
                             'ticker': 'USD', 'type': 'MARKET',
                             'quantity': position,
                             'action': 'SELL'}, session)
                elif position < 0:
                    om.bulk_submit({
                             'ticker': 'USD', 'type': 'MARKET',
                             'quantity': abs(position),
                             'action': 'BUY'}, session)
                    
            # if RITC then hedge currency risk
            # wait for above market orders to execute
            # TODO: do no rely on wait, wait for position to close out
# ============================================================================
            
                    
# =============================================================================
    else:
        om.decline_tender(offer['tender_id'], session)
   
def run(name, session):
    cfg = algo.config
    strategy_params = cfg[name]

    offers = om.get_tender(session)
    for offer in offers:
        # process tender
        logging.getLogger(name).info("tender offer is: %s" % (offer))
        ticker = offer['ticker']
        book = om.get_order_book(ticker, session, limit=50)
        dfbook = utils.parse_book(book)
        execute_tender(offer, dfbook, strategy_params, session)

    
if __name__ == '__main__':
    name = 'algo.strategy.tenderoffer'
    run(name)
