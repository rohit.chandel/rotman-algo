"""
Algo utilities.
"""
# core modules
import pandas as pd
import signal
import logging

# algo modules
import algo

class SessionManager:
    """
    Session manager to check market status and handle ctrl^c like signals
    """
    def __init__(self, name, session):
        self.running = True
        signal.signal(signal.SIGINT, self.signal_handler)
        self.name = name
        self.client = algo.config['client']
        self.session = session
        self.market_open = True
    
    def signal_handler(self, frame, signum):
                # register default signal handler
        signal.signal(signal.SIGINT, signal.SIG_DFL)
        logging.getLogger(
                __name__).critical('Shutting down system!')
        self.running = False
    
    def is_market_open(self):
        url = self.client + '/case'
        resp = self.session.get(url)
        if resp.ok:
            case = resp.json()
            self.market_open = case['status'] == 'ACTIVE'

            return case['status'] == 'ACTIVE'
        self.market_open = False
        return self.market_open
    
    def is_running(self):
        return self.running and self.is_market_open()



def parse_book(ob):
    """
    parse order book json to df
    returns:
        pd.DataFrame: ['BIDS', 'BIDSIZE', 'BIDSCUMVOL', 'BIDSVWAP', 'ASKS', 'SIZE', 'ASKSCUMVOL' 'ASKSVWAP']
        
    Uses output of om.get_order_book
    """
    # TODO: fill with zero rather than trimming if sides are unequal
    size = min(len(ob['bids']), len(ob['asks']))
    dfbook = pd.DataFrame.from_dict({
            'BIDS': [ order['price'] for order in ob['bids'][:size]],
            'BIDSSIZE': [ (order['quantity'] - order['quantity_filled']) for order in ob['bids'][:size]],
            'ASKS': [ order['price'] for order in ob['asks'][:size]],
            'ASKSSIZE': [ (order['quantity'] - order['quantity_filled']) for order in ob['asks'][:size]]
            })
    dfbook['BIDSCUMVOL'] = dfbook['BIDSSIZE'].cumsum()
    dfbook['ASKSCUMVOL'] = dfbook['ASKSSIZE'].cumsum()
    dfbook['BIDSVWAP'] = ((dfbook['BIDSSIZE']* dfbook['BIDS']).cumsum())/dfbook['BIDSCUMVOL']
    dfbook['ASKSVWAP'] = ((dfbook['ASKSSIZE']* dfbook['ASKS']).cumsum())/dfbook['ASKSCUMVOL']
    return dfbook


    
    
def parse_book_optimised(ob):
    """
    parse order book json to df
    returns:
        pd.DataFrame: ['BIDS', 'BIDSIZE', 'BIDSCUMVOL', 'BIDSVWAP', 'ASKS', 'SIZE', 'ASKSCUMVOL' 'ASKSVWAP']
        
    Uses output of om.get_order_book
    """
    # TODO: fill with zero rather than trimming if sides are unequal
    dfbids = pd.DataFrame(ob['bids'])
    dfasks = pd.DataFrame(ob['asks'])
    dfbids = dfbids.rename(columns={
            'price': 'BIDS'
            })
    dfasks = dfasks.rename(columns={
            'price': 'ASKS'
            })
    n = min(len(dfbids), len(dfasks))
    dfbids = dfbids[:n]
    dfasks = dfasks[:n]
    dfbids['BIDSSIZE'] = dfbids['quantity'] - dfbids['quantity_filled'] 
    dfasks['ASKSSIZE'] = dfasks['quantity'] - dfasks['quantity_filled'] 
    dfbids = dfbids[['BIDS', 'BIDSSIZE']]
    dfasks = dfasks[['ASKS', 'ASKSSIZE']]
    dfbook = pd.concat([dfbids, dfasks], axis=1)
    dfbook['BIDS'] = dfbook['BIDS'].fillna(method='ffill')
    dfbook['ASKS'] = dfbook['ASKS'].fillna(method='ffill')
    dfbook = dfbook.fillna(0)
    dfbook['BIDSCUMVOL'] = dfbook['BIDSSIZE'].cumsum()
    dfbook['ASKSCUMVOL'] = dfbook['ASKSSIZE'].cumsum()
    dfbook['BIDSVWAP'] = ((dfbook['BIDSSIZE']* dfbook['BIDS']).cumsum())/dfbook['BIDSCUMVOL']
    dfbook['ASKSVWAP'] = ((dfbook['ASKSSIZE']* dfbook['ASKS']).cumsum())/dfbook['ASKSCUMVOL']
    return dfbook