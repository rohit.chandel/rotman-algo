"""
Orders manager module
This module is used to place buy and sell orders and get latest state 
of order book.
"""
# core codules
import logging
import pandas as pd
# rotman modules
import algo
import time

def submit(params, session):
    """ send order
    Args:
        params: order params dictionary
        session: http session object
    Returns:
        boolean: True on success or False of failure
    """
    cfg = algo.config
    client =  cfg['client']
    url = client + '/orders'
    resp = session.post(url, params=params)
    if resp.ok:
        order_ack = resp.json()
        id = order_ack['order_id']
        logging.getLogger(__name__).info('Order successfully '
                          'executed with params %s and ID %s' %
                          (params, id))
        return order_ack
    else:
        logging.getLogger(__name__).critical('Order failed with params %s and'
                          'msg %s'
                          % (params, resp.json()))
        return False


def bulk_submit(params, session, retries=5):
    order_size = params['quantity']
    # TODO: dynamically read limits from api
    order_limit = 10000
    if params['ticker'] == 'USD':
        order_limit = 2500000
    tries = 0
    orders_acks = []
    while order_size > order_limit:
        params['quantity'] = order_limit
        status = submit(params, session)
        if status:
            order_size = order_size - order_limit
        else:
            tries += 1
            if tries > retries:
                logging.getLogger(__name__).critical('Unable to place bulk orders')
                return False
        time.sleep(0.1)
    if order_size > 0 and order_size <= order_limit:
        params['quantity'] = order_size
        status = submit(params, session)
        return status
    return True

def cancel(order_id, session):
    """ cancel order
    Args:
        order_id: order id to be cancelled
        session: http session object
    Returns:
        boolean: True on success or False on failure
    """
    cfg = algo.config
    client = cfg['client']
    url = client + '/orders'
    resp = session.delete('%s/%s' % (url, order_id))
    if resp.ok:
        status = resp.json()
        success = status['success']
        logging.getLogger(
                __name__).info('The order %s '
                        ' is cancelled successfully  %s.' % (order_id,
                                                             success))
        return True
    else:
        logging.getLogger(__name__).critical('Order id %s cancellation '
                         ' failed with message %s'
                          % (id, resp.json()))
        return False
    

def bulk_cancel(session):
    cfg = algo.config
    client = cfg['client']
    url = client + '/commands/cancel'
    resp = session.post(url, params={'all': 1, 'query': 'Price>0'})
    if not resp.ok:
        logging.getLogger(__name__).critical('cancellation '
                         ' failed with message %s'
                          % (resp.json()))
    return resp.ok

def get_trade_history(ticker, session):
    cfg = algo.config
    client = cfg['client']
    url = client + '/securities/tas'
    resp = session.get(url, params={'ticker': ticker})
    if resp.ok:
        df = pd.DataFrame(resp.json())
        if df.empty:
            return None
        df['price_vol'] = df['price']*df['quantity']
        dfgrouped = df.groupby('tick').agg({'price_vol': 'sum', 'quantity': 'sum'})
        dfout = dfgrouped.reset_index()
        dfout['vwap'] = dfout['price_vol']/dfout['quantity']
        dfout = dfout.sort_values('tick')
        return dfout
    return None
    
  
def get_order_book(ticker, session, limit=20):
    """
    get latest order book state
    """
    params = {'ticker': ticker, 'limit': limit}
    cfg = algo.config
    client = cfg['client']
    url = client + '/securities/book'
    resp = session.get(url, params=params)
    if resp.ok:
        return resp.json()
    return None 


def get_tender(session):
    """
    get tender info
    """
    cfg = algo.config
    client = cfg['client']
    url = client + '/tenders'
    resp = session.get(url)
    if resp.ok:
        return resp.json()
    return []


def accept_tender(tender_id, session):
    cfg = algo.config
    client = cfg['client']
    url = client + '/tenders/' + str(tender_id)
    resp = session.post(url)
    return resp.ok

def decline_tender(tender_id, session):
    cfg = algo.config
    client = cfg['client']
    url = client + '/tenders/' + str(tender_id)
    resp = session.delete(url)
    return resp.ok


def sec_info(session,ticker=None):
    """
    get last price of security
    """
    params = None
    if ticker:
        params = {'ticker': ticker}
    cfg = algo.config
    client = cfg['client']
    url = client + '/securities'
    resp = session.get(url, params=params)
    if resp.ok:
        return resp.json()
    return None 

def sec_info_k(ticker,session):
    """
    get last price of security
    """
    params = None
    if ticker:
        params = {'ticker': ticker}
    cfg = algo.config
    client = cfg['client']
    url = client + '/securities'
    resp = session.get(url, params=params)
    if resp.ok:
        return resp.json()
    return None 

def get_orders(session):
    cfg = algo.config
    client = cfg['client']
    url = client + '/orders'
    resp = session.get(url)
    if resp.ok:
        return resp.json()
    return None  

def order_deets(session,id):
    cfg = algo.config
    client = cfg['client']
    url = client + '/orders/' + str(id)
    resp = session.get(url)
    if resp.ok:
        return resp.json()
    return None  

def TAS(session,ticker):
    """
    returns entire TAS so filter for tick
    """
    params = {'ticker':ticker}
    cfg = algo.config
    client = cfg['client']
    url = client + '/securities/tas'
    resp = session.get(url, params=params)
    if resp.ok:
        return resp.json()[0]
    return None 

def cancel_all_orders(session):
    cfg = algo.config
    client = cfg['client']
    url = client + '/orders'
    resp = session.get(url)
    if resp.ok:
        ids = [x['order_id'] for x in resp.json()]
    try:
        for id in ids:
            session.delete(url +'/'+str(id))
            #print("deleted")
    except:
        next
        #print("could not cancel")
    return None 
            
    
    
    
def get_tick(session):
    cfg = algo.config
    client = cfg['client']
    url = client + '/case'
    resp = session.get(url)
    if resp.ok:
        return resp.json()['tick']
    return None  
    
    
    
if __name__ == "__main__":
    params = {'ticker': 'CRZY_M', 'type': 'MARKET', 'quantity': 1000,
        'action': 'BUY'}
    submit(params)