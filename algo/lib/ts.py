"""
module for frequent operations on timeseries
"""
# core modules
import pandas as pd 
# algo modules
import algo
from algo.lib.error import AlgoError


TIMESTAMP = 'TIME'

def make_ts(columns=[algo.config['universe']]):
    """
    get empty timeseries
    """
    df = pd.DataFrame(columns=columns + [TIMESTAMP])
    df.set_index(TIMESTAMP, inplace=True)
    return df

def is_ts(df):
    """
    checks if df is a valid time series
    """
    if TIMESTAMP != df.index.name:
        return False
    if not isinstance(df.index, pd.DatetimeIndex):
        return False
    return True

def insert(df, data):
    """
    insert data to timeseries df
    """
    data = pd.DataFrame.from_dict(data)
    data.set_index(TIMESTAMP, inplace=True)
    if not is_ts(data):
        raise AlgoError('Inseting incorrect type to TS object')
    return df.append(data)
    
def rolling_mean(df,
                window=500,
                min_periods=None,
                ):
    """
    Computes rolling mean
    Args:
        df(pd.DataFrame): ts dataframe
        min_period(int): min periods to compute mean
    Returns:
        pd.DataFrame: mean dataframe
    """
    df = df.copy()
    if not is_ts(df):
        raise AlgoError('df should be valid time series dataframe')
    df = df.rolling(window, min_periods=min_periods).mean()
    return df
   
    
def rolling_std(df,
                window=500,
                min_periods=None,
                ):
    """
    Computes rolling std
    Args:
        df(pd.DataFrame): ts dataframe
        window(int): rolling window size
        min_period(int): min periods to compute std
    Returns:
        pd.DataFrame: std dataframe
    """
    df = df.copy()
    if not is_ts(df):
        raise AlgoError('df should be valid time series dataframe')
    df = df.rolling(window, min_periods=min_periods).std()
    return df
       
        