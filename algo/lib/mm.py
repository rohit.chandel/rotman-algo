"""
Helper functions for Market Making
"""

# core codules
import requests
import logging
from time import sleep
# algo modules
import algo
import algo.lib.utils as utils
import algo.lib.om as om
import numpy as np

def limit_enter_RITC(s,ticker,amount, bid_spread,ask_spread):
    """Step 1: Get Mid """ 
    usd = om.sec_info(s,'USD')[0]['last']
    bid_fair = (om.sec_info(s,'BULL')[0]['bid'] + om.sec_info(s,'BEAR')[0]['bid'])/usd   
    ask_fair = (om.sec_info(s,'BEAR')[0]['ask'] + om.sec_info(s,'BULL')[0]['ask']   )/usd
     
    """Step 2: Set Ask and Bid"""
    bid = bid_fair - bid_spread
    ask = ask_fair + ask_spread
    msg_buy  = {'ticker': ticker, 'type': 'LIMIT', 'quantity': amount,
                'action': 'BUY', "price": bid}
    msg_sell  = {'ticker': ticker, 'type': 'LIMIT', 'quantity': amount,
                 'action': 'SELL', "price": ask}
    pos = om.sec_info(s,'RITC')[0]['position']
# =============================================================================
    if pos > 40000:
        om.submit(msg_sell,s)
        return None
    elif pos < -40000:
        om.submit(msg_buy,s)
        return None
    else:
        om.submit(msg_buy,s)
        om.submit(msg_sell,s)
# =============================================================================
    return None
        

def limit_enter(s,ticker,amount, bid_spread,ask_spread):
    """Step 1: Get Mid """ 
    bid_fair = (om.sec_info(s,ticker)[0]['last'] )
    ask_fair = (om.sec_info(s,ticker)[0]['last'] )
     
    """Step 2: Set Ask and Bid"""
    bid = bid_fair - bid_spread
    ask = ask_fair + ask_spread
    msg_buy  = {'ticker': ticker, 'type': 'LIMIT', 'quantity': amount,
                'action': 'BUY', "price": bid}
    msg_sell  = {'ticker': ticker, 'type': 'LIMIT', 'quantity': amount,
                 'action': 'SELL', "price": ask}
    om.submit(msg_buy,s)
    om.submit(msg_sell,s)

def get_orders(s,ticker):
    orders = om.get_orders(s)
    ticker_orders = [order['order_id'] for order in orders if order['ticker'] == ticker]
    return ticker_orders

def convert_all_usd(s):
    usd_pos = om.sec_info_k('USD',s)[0]['position']
    if usd_pos < 0:
        dir_ = 'SELL'
    elif usd_pos > 0:
        dir_ = 'BUY'
    else:
        return None
    
    msg_buy  = {'ticker': 'USD', 'type': 'MARKET', 'quantity': np.abs(usd_pos),
                'action': dir_ }
    om.submit(msg_buy,s)
    return None

def cancel_enter_hedge_RITC(s,ticker_orders,amount):
    if len(ticker_orders) == 1:
        direction = om.order_deets(s,ticker_orders[0])['action']
        om.cancel_all_orders(s)
        if direction == 'BUY':
            hedge_dir = 'SELL'
            side = 'bid'
        else:
            hedge_dir = 'BUY'
            side = 'ask'
        last_usd = om.sec_info_k('RITC',s)[0]['last']
        msg_buy  = {'ticker': 'USD', 'type': 'MARKET', 'quantity': amount*last_usd,
                'action': hedge_dir }
        #convert_all_usd(s)
        om.submit(msg_buy,s)
        return hedge_dir

def cancel_enter_hedge(s,ticker_orders,amount):
    if len(ticker_orders) == 1:
        direction = om.order_deets(s,ticker_orders[0])['action']
        om.cancel_all_orders(s)
        if direction == 'BUY':
            hedge_dir = 'SELL'
            side = 'bid'
        else:
            hedge_dir = 'BUY'
            side = 'ask'
        #convert_all_usd(s)
        #om.submit(msg_buy,s)
        return hedge_dir
# =============================================================================
# 
#         msg = {'ticker': "BULL", 'type': 'MARKET', 'quantity': amount, 'action':  direction}
#         om.submit(msg,s)
#         msg = {'ticker': "BEAR", 'type': 'MARKET', 'quantity': amount, 'action':  direction}
#         om.submit(msg,s)
# =============================================================================
