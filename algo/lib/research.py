"""
Research Module
This module is used to download time series for research. 

"""
# core codules
import logging
# rotman modules
import algo
import requests
# algo modules
from time import sleep
import algo.lib.utils as utils
import algo.lib.om as om
import pandas as pd
import numpy as np



def VWAP(s,ticker,size,side = 0):
    """
    0 for bid 1 for ask
    """
    ob = om.get_order_book(ticker,s)
    dfbook = utils.parse_book(ob)
    if side == 0:
        dfbook = dfbook[ dfbook['BIDSCUMVOL'] >= size]
        if not dfbook.empty:
            return dfbook.iloc[0]['BIDSVWAP']
    elif side == 1:
        dfbook = dfbook[ dfbook['ASKSCUMVOL'] >= size ]
        if not dfbook.empty:
            return dfbook.iloc[0]['ASKSVWAP']
    return None 

def BID(session,ticker):
    params = {'ticker':ticker}
    cfg = algo.config
    client = cfg['client']
    url = client + '/securities'
    resp = session.get(url, params=params)
    if resp.ok:
        return resp.json()[0]['bid']
    return None 

def ASK(session,ticker):
    params = {'ticker':ticker}
    cfg = algo.config
    client = cfg['client']
    url = client + '/securities'
    resp = session.get(url, params=params)
    if resp.ok:
        return resp.json()[0]['ask']
    return None 

def LAST(session,ticker):
    params = {'ticker':ticker}
    cfg = algo.config
    client = cfg['client']
    url = client + '/securities'
    resp = session.get(url, params=params)
    if resp.ok:
        return resp.json()[0]['last']
    return None 

def TOTAL_VOLUME(session,ticker):
    params = {'ticker':ticker}
    cfg = algo.config
    client = cfg['client']
    url = client + '/securities'
    resp = session.get(url, params=params)
    if resp.ok:
        return resp.json()[0]['total_volume']
    return None 

def history(session,ticker,tick):
    """
    returns entire TAS so filter for tick
    """
    params = {'ticker':ticker}
    cfg = algo.config
    client = cfg['client']
    url = client + '/securities/history'
    resp = session.get(url, params=params)
    if resp.ok:
        TAS_master = resp.json()
        ticks = [x['ticks'] for i in TAS_master]
        open_ = [x['open'] for i in TAS_master]
        high_ = [x['high'] for i in TAS_master]
        low_ = [x['low'] for i in TAS_master]
        close_ = [x['close'] for i in TAS_master]
        return [ticks,open_,high_,low_,close_]
    return None 

def TAS(session,ticker,tick):
    """
    returns entire TAS so filter for tick
    """
    params = {'ticker':ticker}
    cfg = algo.config
    client = cfg['client']
    url = client + '/securities/tas'
    resp = session.get(url, params=params)
    if resp.ok:
        TAS_master = resp.json()
        ticks = [x['tick'] for i in TAS_master]
        price_ = [x['price'] for i in TAS_master]
        quantity_ = [x['quantity_'] for i in TAS_master]
        return [ticks,price_,quantity_]    
    return None 

def tender_find(session):
    # params = {'ticker':ticker}
    offer = om.get_tender(session, 0.1)[0]
    id_ = offer['tender_id']
    tick = offer['tick']
    deets = [offer['quantity'],offer['action'],offer['price']]
    return [id_,tick,deets]


def run_research():
    cfg = algo.config
    api_key = {'X-API-key': cfg['api_key']}
    with requests.Session() as s:
        s.headers.update(api_key)
        sm = utils.SessionManager(__name__, s)
        count = 15
        while True:

            ticks_passed  = []
            tenders_set = []
            tender_deets = []
            BULL_BID, BEAR_BID, RITC_BID = [],[],[]
            BULL_ASK, BEAR_ASK, RITC_ASK = [],[],[]
            BULL_LAST, BEAR_LAST, RITC_LAST = [],[],[]
            BULL_TOTAL_VOLUME, BEAR_TOTAL_VOLUME, RITC_TOTAL_VOLUME = [],[],[]
            BULL_VWAP_BID_1000, BEAR_VWAP_BID_1000, RITC_VWAP_BID_1000 = [],[],[]
            BULL_VWAP_BID_10000, BEAR_VWAP_BID_10000, RITC_VWAP_BID_10000 = [],[],[]
            BULL_VWAP_BID_20000, BEAR_VWAP_BID_20000, RITC_VWAP_BID_20000 = [],[],[]
            BULL_VWAP_BID_50000, BEAR_VWAP_BID_50000, RITC_VWAP_BID_50000 = [],[],[]   
            BULL_VWAP_ASK_1000, BEAR_VWAP_ASK_1000, RITC_VWAP_ASK_1000 = [],[],[]
            BULL_VWAP_ASK_10000, BEAR_VWAP_ASK_10000, RITC_VWAP_ASK_10000 = [],[],[]
            BULL_VWAP_ASK_20000, BEAR_VWAP_ASK_20000, RITC_VWAP_ASK_20000 = [],[],[]
            BULL_VWAP_ASK_50000, BEAR_VWAP_ASK_50000, RITC_VWAP_ASK_50000 = [],[],[] 
            print("paused for now")                   
            sleep(1)

            while sm.is_running():
                print("running")
                sleep(1)
                tick = om.get_tick(s)
                if tick not in ticks_passed:
                    #price data 
                    ticks_passed.append(tick)
                    BULL_BID.append( BID(s,"BULL"))
                    BEAR_BID.append( BID(s,"BEAR"))
                    RITC_BID.append( BID(s,"RITC"))
                    BULL_ASK.append( ASK(s,"BULL"))
                    BEAR_ASK.append( ASK(s,"BEAR"))
                    RITC_ASK.append( ASK(s,"RITC"))
                    BULL_LAST.append( LAST(s,"BULL"))
                    BEAR_LAST.append( LAST(s,"BEAR"))
                    RITC_LAST.append( LAST(s,"RITC"))
                    BULL_TOTAL_VOLUME.append( TOTAL_VOLUME(s,"BULL"))
                    BEAR_TOTAL_VOLUME.append( TOTAL_VOLUME(s,"BEAR"))
                    RITC_TOTAL_VOLUME.append( TOTAL_VOLUME(s,"RITC"))                        
                    BULL_VWAP_BID_1000.append(VWAP(s,"BULL",1000,0)) 
                    BULL_VWAP_BID_10000.append(VWAP(s,"BULL",10000,0))
                    BULL_VWAP_BID_20000.append(VWAP(s,"BULL",20000,0))
                    BULL_VWAP_BID_50000.append(VWAP(s,"BULL",50000,0))
                    BEAR_VWAP_BID_1000.append(VWAP(s,"BEAR",1000,0)) 
                    BEAR_VWAP_BID_10000.append(VWAP(s,"BEAR",10000,0))
                    BEAR_VWAP_BID_20000.append(VWAP(s,"BEAR",20000,0))
                    BEAR_VWAP_BID_50000.append(VWAP(s,"BEAR",50000,0))
                    RITC_VWAP_BID_1000.append(VWAP(s,"RITC",1000,0)) 
                    RITC_VWAP_BID_10000.append(VWAP(s,"RITC",10000,0))
                    RITC_VWAP_BID_20000.append(VWAP(s,"RITC",20000,0))
                    RITC_VWAP_BID_50000.append(VWAP(s,"RITC",50000,0))
                    BULL_VWAP_ASK_1000.append(VWAP(s,"BULL",1000,1)) 
                    BULL_VWAP_ASK_10000.append(VWAP(s,"BULL",10000,1))
                    BULL_VWAP_ASK_20000.append(VWAP(s,"BULL",20000,1))
                    BULL_VWAP_ASK_50000.append(VWAP(s,"BULL",50000,1))
                    BEAR_VWAP_ASK_1000.append(VWAP(s,"BEAR",1000,1)) 
                    BEAR_VWAP_ASK_10000.append(VWAP(s,"BEAR",10000,1))
                    BEAR_VWAP_ASK_20000.append(VWAP(s,"BEAR",20000,1))
                    BEAR_VWAP_ASK_50000.append(VWAP(s,"BEAR",50000,1))
                    RITC_VWAP_ASK_1000.append(VWAP(s,"RITC",1000,1)) 
                    RITC_VWAP_ASK_10000.append(VWAP(s,"RITC",10000,1))
                    RITC_VWAP_ASK_20000.append(VWAP(s,"RITC",20000,1))
                    RITC_VWAP_ASK_50000.append(VWAP(s,"RITC",50000,1))
# =============================================================================
#                     tender = tender_find(s)
#                     try:
#                         if tender[0] not in tenders_set:
#                             tenders_set.append(tender[0])
#                             tender_deets.append(tender[1:])
#                         else:
#                             tender_deets.append('')
#                     except:
#                         print('no t')
# =============================================================================
            if BULL_BID != []:
                BULL_data = pd.DataFrame()
                BEAR_data = pd.DataFrame()
                RITC_data = pd.DataFrame()
                Tender_data = pd.DataFrame() 
    
    
                BULL_data['Tick'] = ticks_passed
                BULL_data['BID'] = BULL_BID
                BULL_data['ASK'] = BULL_ASK
                BULL_data['LAST']= BULL_LAST
                BULL_data['TOTAL_VOLUME']= BULL_TOTAL_VOLUME
                BULL_data['BID_VWAP_1000']= BULL_VWAP_BID_1000
                BULL_data['BID_VWAP_10000']= BULL_VWAP_BID_10000
                BULL_data['BID_VWAP_20000']= BULL_VWAP_BID_20000
                BULL_data['BID_VWAP_50000']= BULL_VWAP_BID_50000
                BULL_data['ASK_VWAP_1000']= BULL_VWAP_ASK_1000
                BULL_data['ASK_VWAP_10000']= BULL_VWAP_ASK_10000
                BULL_data['ASK_VWAP_20000']= BULL_VWAP_ASK_20000
                BULL_data['ASK_VWAP_50000']= BULL_VWAP_ASK_50000
    
                BEAR_data['Tick'] = ticks_passed
                BEAR_data['BID'] = BEAR_BID
                BEAR_data['ASK'] = BEAR_ASK
                BEAR_data['LAST']= BEAR_LAST
                BEAR_data['TOTAL_VOLUME']= BEAR_TOTAL_VOLUME
                BEAR_data['BID_VWAP_1000']= BEAR_VWAP_BID_1000
                BEAR_data['BID_VWAP_10000']= BEAR_VWAP_BID_10000
                BEAR_data['BID_VWAP_20000']= BEAR_VWAP_BID_20000
                BEAR_data['BID_VWAP_50000']= BEAR_VWAP_BID_50000
                BEAR_data['ASK_VWAP_1000']= BEAR_VWAP_ASK_1000
                BEAR_data['ASK_VWAP_10000']= BEAR_VWAP_ASK_10000
                BEAR_data['ASK_VWAP_20000']= BEAR_VWAP_ASK_20000
                BEAR_data['ASK_VWAP_50000']= BEAR_VWAP_ASK_50000
    
                RITC_data['Tick'] = ticks_passed
                RITC_data['BID'] = RITC_BID
                RITC_data['ASK'] = RITC_ASK
                RITC_data['LAST']= RITC_LAST
                RITC_data['TOTAL_VOLUME']= RITC_TOTAL_VOLUME
                RITC_data['BID_VWAP_1000']= RITC_VWAP_BID_1000
                RITC_data['BID_VWAP_10000']= RITC_VWAP_BID_10000
                RITC_data['BID_VWAP_20000']= RITC_VWAP_BID_20000
                RITC_data['BID_VWAP_50000']= RITC_VWAP_BID_50000
                RITC_data['ASK_VWAP_1000']= RITC_VWAP_ASK_1000
                RITC_data['ASK_VWAP_10000']= RITC_VWAP_ASK_10000
                RITC_data['ASK_VWAP_20000']= RITC_VWAP_ASK_20000
                RITC_data['ASK_VWAP_50000']= RITC_VWAP_ASK_50000
    
# =============================================================================
#                 Tender_data['Tick'] = ticks_passed
#                 Tender_data['deets'] = tender_deets
# =============================================================================
    
                BULL_data.to_csv('../historical_data/BULL_data' + str(count) + '.csv')
                BEAR_data.to_csv('../historical_data/BEAR_data' + str(count) + '.csv')
                RITC_data.to_csv('../historical_data/RITC_data' + str(count) + '.csv')
# =============================================================================
#                 Tender_data.to_csv('../historical_data/Tender_data' + str(count) + '.csv')
# =============================================================================
    
                count += 1 

                    #tender info 
                # print(TAS(s,"RITC",1))
                # print(TAS(s,"RITC",1))
    


if __name__ == '__main__':
    run_research()