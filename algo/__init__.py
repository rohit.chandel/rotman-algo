import json
import os
import logging

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

with open(os.path.join(ROOT_DIR, 'algo.json')) as config_file:
    config = json.load(config_file)
    
    # set global  basic settings of logger
    level = logging.CRITICAL if config['logging_level'] == 'critical' else logging.INFO
    logging.basicConfig(
        format='%(asctime)s %(levelname)-8s %(message)s',
        level=level,
        datefmt='%Y-%m-%d %H:%M:%S')


