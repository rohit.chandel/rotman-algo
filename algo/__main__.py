"""
main module to run all strategies

"""
import requests
import time
# algo modules
import algo
import algo.lib.utils as utils
#import algo.lib.om as om
import algo.lib.ts as ts
import algo.lib.om as om
import algo.strategy.tenderoffer as tenderoffer
import algo.strategy.etfarbitrage as etfarb
import algo.strategy.etf_arb as ea
import algo.strategy.momentum as mom
import algo.strategy.etfarb_converters as etfarb_converters
import algo.strategy.algo_mm as mm
import pandas as pd

if __name__ == '__main__':
    cfg = algo.config
    api_key = {'X-API-key': cfg['api_key']}
    cfg = algo.config
    with requests.Session() as s:
        s.headers.update(api_key)
        sm = utils.SessionManager(__name__, s)
        i = 0
        while True:
            if sm.is_running():
                i += 1
                if i > 5:
                    tenderoffer.run('algo.strategy.tenderoffer', s)
#                    etfarb.run('algo.strategy.etfarbitrage', s)
                    mm.run('algo.strategy.algo_mm', s, trade_amount=5000, penalty=0.25, bull_spread=0.01, bear_spread=0.01)
                    
#                    dfsec = pd.DataFrame(om.sec_info(s))
#                    usd_pos = dfsec.loc[dfsec['ticker'] == 'USD', 'position'].values[0]
#                        # hedge currency
#                    if usd_pos < 0 and abs(usd_pos) > 5000000:
#                        om.bulk_submit({'ticker': 'USD', 'type': 'MARKET', 'quantity': abs(usd_pos)/2,
#                                        'action': 'BUY'}, s)
#                    elif  usd_pos > 0 and abs(usd_pos) > 5000000:
#                        om.bulk_submit({'ticker': 'USD', 'type': 'MARKET', 'quantity': abs(usd_pos)/2,
#                                        'action': 'SELL'}, s)                    
            if sm.market_open == False:
                i = 0
                etfarb_converters.init()
            if sm.running == False:
                break
            time.sleep(0.5)
            

            